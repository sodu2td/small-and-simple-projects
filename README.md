# Small and Simple projects
These are projects that are made in python. These are small projects that I made in my free time. Some of these are usefull while some are not so much...
## Requirements
You need to have python installed. To install got to https://www.python.org/downloads/ and download the latest version. This python repo was made with the version 3.9 so If there are any new versions there might be a difference or a bug. And while I dont think there might be a problem, if there is  then go to the issues tab and tell us your issue!
## How to download
To download, make sure that you have git installed... 
### Installing Git
If you don't have git installed then go to https://git-scm.com/downloads and download the version that is for your OS. It will automatically choose what your os is but if it is not the same os as you have or you want to download another type of os, then git does have downloads for Windows, Mac, and linux. Which I think 99% of you will have. If you are on chromebook, there is still a way. Assuming your chromebook supports android apps, install Termux. And then install git through that.
## Cloning the repo
After you have git installed, you can open git Bash and cd(ex. cd {Name of the directory}, cd desktop, cd programming, cd gitlab repositories, etc.) in to the directory you want to download the file, then type:
git clone https://gitlab.com/sodu2td/mall-and-simple-projects.git


That should clone the repo in the directory. 

## How to use
To use, simply click on the folder the interests you. And then there should be ANOTHER README.md file which should tell you on how to use the project.

## License
This repo follows the MIT license.
